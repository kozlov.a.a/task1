<?php

if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', realpath(__DIR__.'/../'));
}

require ROOT_PATH . '/vendor/autoload.php';

$config = json_decode(
    str_replace(
        '%ROOT_PATH%',
        ROOT_PATH,
        file_get_contents(ROOT_PATH . '/config/config.json')
    ),

    true
);
$app = new \Silex\Application();

require ROOT_PATH . '/src/dependencies.php';

$app->register(new \App\Providers\AssetProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), $config['twig']);

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$routesLoader = new App\RoutesLoader($app);
$routesLoader->bindRoutesToControllers();

//$app->register(new Silex\Provider\SecurityServiceProvider());
//
//$app['security.firewalls'] = [
//    'secured' => [
//        'pattern' => '^.*$',
//        'form' => ['login_path' => '/user/login', 'check_path' => '/user/login_check'],
//        'anonymous' => true,
//        'users' => function () use ($app) {
//            return new UserProvider($app['db']);
//        },
//
//    ]
//];

$app->run();
