<?php

namespace App\Providers;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\Packages;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

/**
 * Class AssetProvider
 * @package App\Providers
 */
class AssetProvider implements ServiceProviderInterface
{


    /**
     * HttpClientProvider constructor.
     */
    public function __construct()
    {
    }

    /**
     * @inheritdoc
     */
    public function register(Container $pimple)
    {
        $pimple['assets.packages'] = function () {

            $version = new EmptyVersionStrategy();
            $default = new Package($version);

            $namedPackages = array(
                'bootstrap-css' => new UrlPackage('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/', $version),
                'bootstrap-js'  => new UrlPackage('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/', $version),
                'jquery-js'     => new UrlPackage('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/', $version),
            );

            return new Packages($default, $namedPackages);
        };
    }
}
