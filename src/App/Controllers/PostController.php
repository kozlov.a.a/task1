<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 9:10
 */

namespace App\Controllers;

use App\Entities\Category;
use App\Entities\Post;
use App\Repositories\PostRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostController
 * @package App\Controllers
 */
class PostController extends BaseController
{
    const LIMIT_PER_PAGE = 20;

    public function getAll(Request $request)
    {
        $limit = $request->get('limit') ?? self::LIMIT_PER_PAGE;
        $start = $request->get('startId') ?? 0;
        return $this->renderTemplate('posts/all.html.twig', [
            'posts'         => $this->getPostRepository()->getPosts($start, $limit+1),
            'limit'         => $limit,
            'user'          => $this->getCurrentUser($request),
            'categories'    => $this->getEntityManager()->getRepository(Category::class)->findAll(),
        ]);
    }

    public function addPost(Request $request)
    {
        $category = $this->getCategoryRepository()
            ->find($request->get('categoryId'));
        if (null === $category) {
            throw new NotFoundHttpException('category not found');
        }

        $newPost = new Post();
        $newPost->setText($request->get('text'));
        $newPost->setCategory($category);
        $this->getEntityManager()->persist($newPost);
        $this->getEntityManager()->flush();
        return new RedirectResponse('/post/all');
    }

    /**
     * @return PostRepository
     */
    private function getPostRepository(): PostRepository
    {
        return $this->getEntityManager()
            ->getRepository(Post::class);
    }

    /**
     * @return PostRepository
     */
    private function getCategoryRepository(): EntityRepository
    {
        return $this->getEntityManager()
            ->getRepository(Category::class);
    }
}
