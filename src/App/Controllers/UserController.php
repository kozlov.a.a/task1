<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 9:10
 */

namespace App\Controllers;

use App\Entities\User;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package App\Controllers
 */
class UserController extends BaseController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function toggleLike(Request $request)
    {
        $user = $this->getCurrentUser($request);
        return $this->getEntityManager()
            ->getRepository(User::class)
            ->toggleLike($user, $request->get('postId'));
    }
}