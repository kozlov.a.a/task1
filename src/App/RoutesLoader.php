<?php
declare(strict_types=1);

namespace App;

use App\Controllers\PostController;
use App\Controllers\UserController;
use Silex\Application;

/**
 * Load routes
 *
 * Class RoutesLoader
 * @package App
 */
class RoutesLoader
{

    /**
     * @var Application
     */
    private $app;

    /**
     * RoutesLoader constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();
    }

    /**
     * Init controller classes
     */
    private function instantiateControllers(): void
    {
        $this->app['controller.user'] = $this->registerController(UserController::class);
        $this->app['controller.post'] = $this->registerController(PostController::class);

    }

    /**
     * Bind routes to controllers
     *
     * @throws \LogicException
     * @SuppressWarnings(PHPMD.UnusedVariable)
     */
    public function bindRoutesToControllers(): void
    {
        $this->app->get('/post/all', 'controller.post:getAll');
        $this->app->post('/post/addPost', 'controller.post:addPost');
        $this->app->post('/user/toggleLike', 'controller.user:toggleLike');
    }

    /**
     * Register controller
     *
     * @param $class
     * @return \Closure
     */
    public function registerController($class): \Closure
    {
        return function () use ($class) {
            $controller = new $class(
                $this->app['orm.em'],
                $this->app['twig']
            );

            return $controller;
        };
    }
}
