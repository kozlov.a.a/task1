<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 0:28
 */

namespace App\Repositories;

use App\Entities\Post;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Entities\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends EntityRepository
{
    /**
     * @var ResultSetMapping
     */
    private $rsm;

    /**
     * @param int $startId
     * @param $limit
     * @return array
     */
    public function getUsers(int $startId, $limit): array
    {
        return $this->getEntityManager()
            ->createNativeQuery(
                'select id, email from users where id > :id limit :limit', $this->getRsm()
            )
            ->setParameters(['id' => $startId, 'limit' => $limit])
            ->getResult();
    }

    /**
     * @return ResultSetMapping
     */
    private function getRsm()
    {
        if (null === $this->rsm) {
            $this->rsm = new ResultSetMapping;
            $this->rsm->addEntityResult(User::class, 'u');
            $this->rsm->addFieldResult('u', 'id', 'id');
            $this->rsm->addFieldResult('u', 'email', 'email');
        }
        return $this->rsm;
    }

    /**
     * @param User $user
     * @param $postId
     * @return bool
     */
    public function toggleLike(User $user, $postId): bool
    {
        $post = $this->getPost($postId);
        if(!$user->getLikedPosts()->contains($post)) {
            $this->addLike($user, $post);
        } else {
            $this->removeLike($user, $post);
        }
        return true;
    }

    /**
     * @param User $user
     * @param Post $post
     */
    public function addLike(User $user, Post $post)
    {
        $user->getLikedPosts()->add($post);
        $this->getEntityManager()->flush();
    }

    /**
     * @param User $user
     * @param Post $post
     */
    public function removeLike(User $user, Post $post)
    {
        $user->getLikedPosts()->removeElement($post);
        $this->getEntityManager()->flush();
    }

    /**
     * @param $postId
     * @return Post
     * @throws EntityNotFoundException
     */
    public function getPost($postId): Post
    {
        $post = $this->getEntityManager()
            ->getRepository(Post::class)
            ->find($postId);
        if (null === $post) {
            throw new EntityNotFoundException('');
        }

        return $post;
    }
}
