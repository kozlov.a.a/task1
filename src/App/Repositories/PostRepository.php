<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 10:03
 */

namespace App\Repositories;

use App\Entities\Post;
use Doctrine\ORM\EntityRepository;

/**
 * Class PostRepository
 * @package App\Repositories
 */
class PostRepository extends EntityRepository
{
    /**
     * @param int $startId
     * @param int $limit
     * @return Post[]
     */
    public function getPosts(int $startId, int $limit): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.id > :id')
            ->orderBy('p.id', 'DESC')
            ->setParameter('id', $startId)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
