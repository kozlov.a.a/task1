<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 1:59
 */

namespace App\Commands;

use App\Entities\Category;
use App\Entities\Post;
use App\Entities\User;
use Doctrine\ORM\EntityManager;
use Faker\Generator;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory;

class GenerateFakeDataCommand extends Command
{
    /**
     * Name command in command line
     */
    const
        COMMAND_NAME = 'users:generate-data',
        LIMIT_BATCH  = 100000;

    /**
     * @inheritdoc
     * @throws \InvalidArgumentException
     */
    protected function configure(): Command
    {
        return $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('calculate count domains');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $faker = Factory::create();
        $this->generateUsers($faker, 1000, $output);
        $this->generateCategory($faker, 10, $output);
    }

    /**
     * @param Generator $faker
     * @param int $limitUsers
     * @param OutputInterface $output
     */
    private function generateUsers(Generator $faker, int $limitUsers, OutputInterface $output): void
    {
        for ($i = 1; $i <= $limitUsers; $i++) {
            $user = new User();
            $user->setName($faker->name());
            $user->setGender(random_int(0, 1));

            $emails = [];
            $emailsCount = random_int(1, 5);
            for ($j = 0; $j <= $emailsCount; $j++) {
                $emails[] = $faker->freeEmail;
            }

            $user->setEmail(implode(', ', $emails));
            $this->getEntityManager()->persist($user);

            if ($i % 1000 === 0) {
                $output->writeln('+ ' . $limitUsers . ' users');
                $this->getEntityManager()->flush();
                $this->getEntityManager()->clear();
            }
        }
    }

    /**
     * @param Generator $faker
     * @param int $limitCategory
     * @param OutputInterface $output
     */
    private function generateCategory(Generator $faker, int $limitCategory, OutputInterface $output): void
    {
        for ($i = 1; $i <= $limitCategory; $i++) {
            $category = new Category();
            $category->setName($faker->text(50));
            $this->getEntityManager()->persist($category);
            $this->generatePosts($faker, 10000, $output, $category);
            $this->getEntityManager()->flush();
            $this->getEntityManager()->clear();

        }
    }

    /**
     * @param Generator $faker
     * @param int $count
     * @param OutputInterface $output
     * @param Category $category
     */
    private function generatePosts(Generator $faker, int $count, OutputInterface $output, Category $category): void
    {
        for ($i = 1; $i <= $count; $i++) {
            $post = new Post();
            $post->setText($faker->text(243));

            $post->setCategory($category);
            $category->getPosts()->add($post);
            $this->getEntityManager()->persist($post);
        }
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager(): EntityManager
    {
        return $this->getHelperSet()
            ->get('em')
            ->getEntityManager();
    }
}
